<?php

namespace App\Imports;

use App\Http\Models\Category;
use App\Http\Models\Influencer;
use App\Http\Models\InfluencerCategory;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class InfluencerInstagramImport implements ToModel, WithStartRow
{
    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if(!$row[2]) {
            return;
        }

        $isInfluencersExists = Influencer::where('ig_link', $row[2])->exists();
        if($isInfluencersExists) {
           return;
        }

        $influencerIGName = str_replace('https://www.instagram.com/', '', $row[2]);
        $influencerIGName = str_replace('https://www|instagram|com/', '', $influencerIGName);
        $influencerIGName = str_replace('|', '.', $influencerIGName);
        $influencerIGName = str_replace('/', '', $influencerIGName);

        $influencer = new Influencer();
        $influencer->ig_link = $row[2];
        $influencer->ig_name = $influencerIGName;
        $influencer->ig_followers = $row[3] !== null ? (int)(str_replace('.', '', $row[3])) : 0;
        $influencer->ig_photo = $row[4] !== null ? (int)(str_replace('.', '', $row[4])) : 0;
        $influencer->ig_photo_carousel = $row[5] !== null ? (int)(str_replace('.', '', $row[5])) : 0;
        $influencer->ig_story = $row[6] !== null ? (int)(str_replace('.', '', $row[6])) : 0;
        $influencer->ig_video = $row[7] !== null ? (int)(str_replace('.', '', $row[7])) : 0;
        $influencer->ig_reels = $row[8] !== null ? (int)(str_replace('.', '', $row[8])) : 0;
        $influencer->event_attendance = $row[9] !== null ? (int)(str_replace('.', '', $row[9])) : 0;
        $influencer->ig_live = $row[10] !== null ? (int)(str_replace('.', '', $row[10])) : 0;
        $influencer->phone = $row[11];
        $influencer->email = $row[12];
        $influencer->save();

        $influencerId = $influencer->id;

        $explodeCategory = explode(',', $row[1]);
        foreach ($explodeCategory as $ctgr) {
            // $ctgr = $ctgr[0] === ' ' ? unset($ctgr[0]) : $ctgr;
            $category = Category::where('name', $ctgr)->first();
            $idCategory = $category->id ?? null;
            if (!$idCategory) {
                $data = new Category();
                $data->name = $ctgr;
                $data->created_by = 1;
                $data->save();

                $idCategory = $data->id;
            }

            InfluencerCategory::create([
                'category_id' => $idCategory,
                'influencer_id' => $influencerId
            ]);
        }

        return;
    }
}
