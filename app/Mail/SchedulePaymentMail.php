<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class SchedulePaymentMail extends Mailable
{
    use Queueable, SerializesModels;

    public $projectName;
    public $projectId;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $id)
    {
        $this->projectName = $data;
        $this->projectId = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Schedule Payment - '. $this->projectName)
            ->markdown('mails.schedule-payment');
    }
}
