<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectInfluencer extends Model
{
    protected $table = 'project_influencers';
    protected $fillable = [
        'project_id',
        'influencer_name',
        'bank_name',
        'bank_branch_name',
        'bank_account',
        'invoice',
        'invoice_number',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function projects()
    {
        return $this->belongsTo('App\Http\Models\Project', 'project_id');
    }

    public function projectPayments()
    {
        return $this->hasMany('App\Http\Models\ProjectInfluencerPayment', 'project_influencer_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }

    public function deletedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
