<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
    protected $fillable = [
        'type',
        'name',
        'notes',
        'budget',
        'start_date',
        'estimate_finish_date',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function projectInfluencers()
    {
        return $this->hasMany('App\Http\Models\ProjectInfluencer', 'project_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }

    public function deletedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
