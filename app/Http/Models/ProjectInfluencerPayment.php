<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectInfluencerPayment extends Model
{
    protected $table = 'project_influencer_payments';
    protected $fillable = [
        'project_influencer_id',
        'payment_number',
        'total',
        'due_date',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function projectInfluencers()
    {
        return $this->belongsTo('App\Http\Models\ProjectInfluencer', 'project_influencer_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }

    public function deletedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
