<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Influencer extends Model
{
    protected $table = 'influencers';
    protected $fillable = [
        'category_id',
        'phone_number',
        'email',
        'ig_name',
        'ig_photo',
        'ig_photo_carousel',
        'ig_story',
        'ig_video',
        'event_attendance',
        'youtube_channel',
        'youtube_video',
        'notes',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function categories()
    {
        return $this->hasMany('App\Http\Models\InfluencerCategory', 'influencer_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }

    public function deletedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
