<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class InfluencerCategory extends Model
{
    const CREATED_AT = null;
    const UPDATED_AT = null;
    
    protected $table = 'influencer_categories';
    protected $fillable = [
        'category_id',
        'influencer_id'
    ];

    public function categories()
    {
        return $this->belongsTo('App\Http\Models\Category', 'category_id');
    }

    public function influencers()
    {
        return $this->belongsTo('App\Http\Models\Influencer', 'influencer_id');
    }
}
