<?php

namespace App\Http\Controllers;

use App\Mail\FinishPaymentMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class FinishTransaction extends Controller
{
    public function __invoke(Request $request)
    {
        try {
            $sendEmail = Mail::to('steven.seventech@gmail.com')
                ->cc(['keshia.sevenads@gmail.com'])->send(new FinishPaymentMail($request->all()));
        } catch (\Throwable $th) {
            Log::info($th);
        }
    }
}
