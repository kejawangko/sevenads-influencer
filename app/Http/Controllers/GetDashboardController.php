<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\ProjectInfluencerPayment;
use App\Http\Models\User;
use App\Mail\ReminderMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class GetDashboardController extends Controller
{
    private $endpoint = 'http://34.101.73.50:8081/';
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $user = Auth::user();
        $type = 'KOL';
        if($user->role === 'dm') {
            $type = 'DIGITAL_MARKETING';
        } else if($user->role === 'superadmin' || $user->role === 'admin') {
            $type = 'ALL';
        }

        // $reminderDay = Carbon::today()->addDays(2)->format('Y-m-d');
        // $items = ProjectInfluencerPayment::join('project_influencers', 'project_influencer_payments.project_influencer_id', '=', 'project_influencers.id')
        //         ->join('projects', 'project_influencers.project_id', '=', 'projects.id')
        //         ->where('due_date', $reminderDay)->where('status', 'not paid');

        // $allPayments = ProjectInfluencerPayment::join('project_influencers', 'project_influencer_payments.project_influencer_id', '=', 'project_influencers.id')
        //         ->join('projects', 'project_influencers.project_id', '=', 'projects.id')
        //         ->where('status', 'not paid')->orderBy('due_date')
        //         ->where('projects.type', 'KOL')->get();
        // if($type !== 'ALL') {
        //     $items = $items->where('projects.type', $type);
        // }

        // $items = $items->get();

        $allPayments = [];
        $items = [];
        $arrView = [
            'all_payments' => $allPayments,
            'items' => $items
        ];
        
        return view('pages.index', $arrView);
    }
}
