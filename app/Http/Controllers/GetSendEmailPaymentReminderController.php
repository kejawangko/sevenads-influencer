<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\ProjectInfluencerPayment;
use App\Http\Models\User;
use App\Mail\ReminderMail;
use Carbon\Carbon;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class GetSendEmailPaymentReminderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $reminderDay = Carbon::today()->addDays(2)->format('Y-m-d');
        $paymentDueDate = ProjectInfluencerPayment::where('due_date', $reminderDay)->exists();
        if ($paymentDueDate) {
            $superadminUser = User::where('role', 'superadmin')->first()->email;
            $adminUser = User::where('role', 'admin')->first()->email;
            $financeUser = User::where('role', 'finance')->first()->email;
            $sendEmail = Mail::to($financeUser)
                ->cc([$superadminUser, $adminUser, 'keshia.sevenads@gmail.com'])->send(new ReminderMail());
        }

        return view('pages.index');
    }
}
