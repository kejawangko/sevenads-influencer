<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Models\Project;
use App\Http\Models\User;
use App\Mail\SchedulePaymentMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class GetProjectSendEmailPaymentScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($type, $id)
    {
        $item = Project::find($id);

        if(!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        try {
            // send to email for payment schedule
            $superadminUser = User::where('role', 'superadmin')->first()->email;
            $adminUser = User::where('role', 'admin')->first()->email;
            $financeUser = User::where('role', 'finance')->first()->email;
            $sendEmail = Mail::to($financeUser)
                ->cc([$superadminUser, $adminUser, 'keshia.sevenads@gmail.com'])->send(new SchedulePaymentMail($item->name, $id));

            return redirect(url()->previous())->with('success', 'Email sent');
        } catch (\Exception $e) {
            Log::info($e);

            return redirect(url()->previous())->with('failed', 'Please check log');
        }
    }
}