<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Models\ProjectInfluencer;
use App\Http\Models\ProjectInfluencerPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostUpdateScheduleInfluencerPaymentProjectInfluencerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $type, $id)
    {
        $item = ProjectInfluencerPayment::find($id);
        if (!$item) {
            return redirect(url()->previous())->with('Data not found');
        }

        if ($item->status === 'paid') {
            return redirect(url()->previous())->with('Cannot edit, payment already paid');
        }

        try {
            DB::beginTransaction();

            ProjectInfluencer::where('id', $item->project_influencer_id)->update([
                'influencer_name' => $request->name,
                'bank_name' => $request->bank_name,
                'bank_branch_name' => $request->bank_branch_name,
                'bank_account' => $request->bank_account,
                'invoice' => $request->invoice,
                'invoice_number' => $request->invoice_number
            ]);

            $data = [];
            $data['total'] = str_replace(',', '', $request->payment);
            $data['due_date'] = Carbon::parse($request->due_date)->format('Y-m-d');
            ProjectInfluencerPayment::where('id', $id)->update($data);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data updated');
    }
}
