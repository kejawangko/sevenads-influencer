<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Models\Project;
use App\Http\Models\ProjectInfluencer;
use App\Http\Models\ProjectInfluencerPayment;

class GetDetailProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($type, $id)
    {
        $item = Project::find($id);
        $type = strtoupper(str_replace('-', ' ', $type));
        $allInfluencer = ProjectInfluencer::select('id')->where('project_id', $id)->get()->pluck('id')->toArray();
        
        $totalOutcome = 0;
        $totalPayout = 0;
        $totalPendingPayment = 0;
        if(count($allInfluencer) > 0) {
            $totalOutcome = ProjectInfluencerPayment::whereIn('project_influencer_id', $allInfluencer)->sum('total');
            $totalPayout = ProjectInfluencerPayment::whereIn('project_influencer_id', $allInfluencer)->where('status', 'paid')->sum('total');
            $totalPendingPayment = ProjectInfluencerPayment::whereIn('project_influencer_id', $allInfluencer)->where('status', 'not paid')->sum('total');
        }

        $arrView = [
            'item' => $item,
            'total_outcome' => $totalOutcome,
            'total_payout' => $totalPayout,
            'total_pending_payment' => $totalPendingPayment,
            'type' => $type
        ];

        return view('pages.projects.detail', $arrView);
    }
}
