<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Models\Project;
use App\Http\Models\ProjectInfluencer;
use App\Http\Models\ProjectInfluencerPayment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetDeleteInfluencerPaymentProjectInfluencerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($type, $id)
    {
        $item = ProjectInfluencerPayment::find($id);
        if (!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        try {
            DB::beginTransaction();

            ProjectInfluencerPayment::where('id', $id)->delete();
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data deleted');

        
    }
}
