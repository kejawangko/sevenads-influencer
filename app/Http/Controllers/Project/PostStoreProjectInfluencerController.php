<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Models\Project;
use App\Http\Models\ProjectInfluencer;
use App\Http\Models\ProjectInfluencerPayment;
use App\Http\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostStoreProjectInfluencerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $type, $id)
    {
        $item = Project::find($id);
        if(!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        $idProjectInfluencer = $request->id_project_influencer;
        try {
            DB::beginTransaction();

            if(!$request->id_project_influencer) {
                $data = new ProjectInfluencer();
                $data->project_id = $id;
                $data->influencer_name = $request->name;
                $data->bank_name = $request->bank_name;
                $data->bank_branch_name = $request->bank_branch_name;
                $data->bank_account = $request->bank_account;
                $data->bank_owner = $request->bank_owner;
                $data->invoice = $request->invoice;
                $data->invoice_number = $request->invoice_number;
                $data->save();
                $idProjectInfluencer = $data->id;
            }

            $schedulePayment = [];
            foreach ($request->payment as $key => $value) {
                $schedulePayment[$key] = [];
                if ($value) {
                    $schedulePayment[$key]['project_influencer_id'] = $idProjectInfluencer;
                    $schedulePayment[$key]['total'] = str_replace(',', '', $value);
                    $schedulePayment[$key]['due_date'] = Carbon::parse($request->due_date[$key])->format('Y-m-d');

                    ProjectInfluencerPayment::create($schedulePayment[$key]);
                }
            }
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data stored');
    }
}
