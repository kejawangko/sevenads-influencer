<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Models\Project;
use App\Http\Models\ProjectInfluencer;
use App\Http\Models\ProjectInfluencerPayment;

class GetProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($type)
    {
        $type = strtoupper(str_replace('-', ' ', $type));
        if ($type !== 'KOL' && $type !== 'DIGITAL MARKETING') {
            $type = 'KOL';
        }

        $items = Project::where('type', $type)->orderByDesc('id')->get();
        foreach ($items as $key => $value) {
            $allInfluencer = ProjectInfluencer::select('id')->where('project_id', $value->id)->get()->pluck('id')->toArray();

            $totalProfit = 0;
            $totalPayout = 0;
            $totalPendingPayment = 0;
            if (count($allInfluencer) > 0) {
                $projectInfluencerPayment = ProjectInfluencerPayment::whereIn('project_influencer_id', $allInfluencer);
                $totalProfit = $projectInfluencerPayment->sum('total');
                $totalPayout = ProjectInfluencerPayment::whereIn('project_influencer_id', $allInfluencer)->where('status', 'paid')->sum('total');
                $totalPendingPayment = ProjectInfluencerPayment::whereIn('project_influencer_id', $allInfluencer)->where('status', 'not paid')->sum('total');
            }
            $value->profit = $value->budget - $totalProfit;
            $value->payout = $totalPayout;
            $value->pending_payment = $totalPendingPayment;
        }

        $arrView = [
            'items' => $items,
            'type' => $type
        ];

        return view('pages.projects.index', $arrView);
    }
}
