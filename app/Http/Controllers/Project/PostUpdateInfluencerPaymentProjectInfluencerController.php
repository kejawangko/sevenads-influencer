<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Models\Project;
use App\Http\Models\ProjectInfluencerPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostUpdateInfluencerPaymentProjectInfluencerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $type, $id)
    {
        $item = ProjectInfluencerPayment::find($id);
        if(!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        if($item->status === 'paid') {
            return redirect(url()->previous())->with('failed', 'Payment already paid');
        }

        $status = 'paid';
        try {
            DB::beginTransaction();
            ProjectInfluencerPayment::where('id', $id)->update(['payment_number' => $request->payment_number, 'status' => $status]);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data updated');
    }
}
