<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostUpdateProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $type, $id)
    {
        try {
            DB::beginTransaction();

            $data = [];
            $data['name'] = $request->name;
            $data['notes'] = $request->notes;
            $data['budget'] = str_replace(',', '', $request->budget);
            $data['start_date'] = $request->start_date;
            $data['estimate_finish_date'] = $request->estimate_finish_date;
            Project::where('id', $id)->update($data);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data updated');
    }
}
