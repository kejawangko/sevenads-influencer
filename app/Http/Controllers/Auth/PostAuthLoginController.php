<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PostAuthLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function __invoke(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return redirect('login')->with('failed', 'Credential not found');
        }

        $checkPassword = Hash::check($request->password, $user->password);
        if (!$checkPassword) {
            return redirect('login')->with('failed', 'Email or password not match');
        }
        if ($user->active === '0') {
            return redirect('login')->with('failed', 'Account disable, please contact admin');
        }

        Auth::login($user);

        if($request->prev_url) {
            return redirect($request->prev_url);
        }
        return redirect('/');
    }
}
