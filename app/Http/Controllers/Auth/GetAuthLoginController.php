<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class GetAuthLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function __invoke()
    {
        $previousUrl = url()->previous();

        $arrView = [
            'prev_url' => $previousUrl
        ];

        return view('pages.auth.login', $arrView);
    }
}
