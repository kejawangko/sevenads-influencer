<?php

namespace App\Http\Controllers\Master\AdminUser;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use App\Mail\NewUserMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class PostMasterStoreAdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $user = Auth::user();
        if ($user->role !== 'superadmin' && $user->role !== 'admin') {
            return redirect('/');
        }
        try {
            DB::beginTransaction();

            $data = new User();
            $data->role = $request->role;
            $data->full_name = $request->name;
            $data->email = $request->email;
            $data->phone_number = $request->phone_number;
            $data->dob = $request->dob;
            $password = 'user_'. strtolower(explode(' ',$request->name)[0]) . '_' . $request->role;
            $data->password = Hash::make($password);
            $data->save();

            $sendEmail = Mail::to($request->email)->send(new NewUserMail($request->name, $password));
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data stored');
    }
}
