<?php

namespace App\Http\Controllers\Master\AdminUser;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use Illuminate\Support\Facades\Auth;

class GetMasterAdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $user = Auth::user();
        if($user->role !== 'superadmin' && $user->role !== 'admin') {
            return redirect('/');
        }
        
        $items = User::get();

        $arrView = [
            'items' => $items
        ];

        return view('pages.master-data.admin-users.index', $arrView);
    }
}
