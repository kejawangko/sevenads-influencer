<?php

namespace App\Http\Controllers\Master\AdminUser;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostMasterUpdateAdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->role !== 'superadmin' && $user->role !== 'admin') {
            return redirect('/');
        }
        try {
            DB::beginTransaction();

            $data = [];
            $data['role'] = $request->role;
            $data['full_name'] = $request->name;
            $data['email'] = $request->email;
            $data['phone_number'] = $request->phone_number;
            $data['dob'] = $request->dob;

            User::where('id', $id)->update($data);
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data stored');
    }
}
