<?php

namespace App\Http\Controllers\Master\Category;

use App\Http\Controllers\Controller;
use App\Http\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostMasterUpdateCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->role !== 'superadmin' && $user->role !== 'admin') {
            return redirect('/');
        }

        $item = Category::find($id);
        if ($item) {
            try {
                DB::beginTransaction();

                $data = [];
                $data['name'] = $request->name;

                Category::where('id', $id)->update($data);
            } catch (\Exception $e) {
                Log::info($e);
                DB::rollBack();

                return redirect('master-data/categories')->withInput()->with('failed', 'Please check log');
            }

            DB::commit();
            return redirect('master-data/categories')->with('success', 'Data updated');
        }

        return redirect(url()->previous())->withInput()->with('failed', 'Data not found');
    }
}
