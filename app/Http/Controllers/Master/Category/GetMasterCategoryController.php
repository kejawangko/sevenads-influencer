<?php

namespace App\Http\Controllers\Master\Category;

use App\Http\Controllers\Controller;
use App\Http\Models\Category;
use Illuminate\Support\Facades\Auth;

class GetMasterCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $user = Auth::user();
        if ($user->role !== 'superadmin' && $user->role !== 'admin') {
            return redirect('/');
        }

        $items = Category::get();

        $arrView = [
            'items' => $items
        ];

        return view('pages.master-data.categories.index', $arrView);
    }
}
