<?php

namespace App\Http\Controllers\Master\Category;

use App\Http\Controllers\Controller;

class GetMasterDeleteCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function __invoke($id)
    {
        //
    }
}
