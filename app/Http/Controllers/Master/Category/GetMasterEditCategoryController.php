<?php

namespace App\Http\Controllers\Master\Category;

use App\Http\Controllers\Controller;
use App\Http\Models\Admin;
use App\Http\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class GetMasterEditCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($id)
    {
        $user = Auth::user();
        if ($user->role !== 'superadmin' && $user->role !== 'admin') {
            return redirect('/');
        }
        
        $item = Category::find($id);

        $arrView = [
            'item' => $item
        ];
        return view('pages.master-data.categories.edit', $arrView);
    }
}
