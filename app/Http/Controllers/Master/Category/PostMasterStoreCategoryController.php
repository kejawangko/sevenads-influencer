<?php

namespace App\Http\Controllers\Master\Category;

use App\Http\Controllers\Controller;
use App\Http\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostMasterStoreCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $user = Auth::user();
        if ($user->role !== 'superadmin' && $user->role !== 'admin') {
            return redirect('/');
        }
        
        try {
            DB::beginTransaction();

            $data = new Category();
            $data->name = $request->name;
            $data->save();
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect(url()->previous())->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect(url()->previous())->with('success', 'Data stored');
    }
}
