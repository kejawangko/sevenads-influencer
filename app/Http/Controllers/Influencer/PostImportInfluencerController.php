<?php

namespace App\Http\Controllers\Influencer;

use App\Http\Controllers\Controller;
use App\Http\Models\Influencer;
use Illuminate\Http\Request;
use App\Imports\InfluencerInstagramImport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class PostImportInfluencerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        try {
            DB::beginTransaction();

            $file = $request->file('import_influencer');
            $nama_file = rand() . $file->getClientOriginalName();
            $file->move('file_influencer', $nama_file);

            Excel::import(new InfluencerInstagramImport, public_path('/file_influencer/' . $nama_file));
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect('influencers')->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect('influencers')->with('success', 'Data imported');
    }
}