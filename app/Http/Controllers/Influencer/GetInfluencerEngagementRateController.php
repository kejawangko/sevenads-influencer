<?php

namespace App\Http\Controllers\Influencer;

use App\Exports\InfluencerExport;
use App\Http\Controllers\Controller;
use App\Http\Models\Category;
use App\Http\Models\Influencer;
use App\Http\Models\InfluencerCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;

class GetInfluencerEngagementRateController extends Controller
{
    private $endpoint = 'http://34.101.251.172:5000/';
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $categories = Category::get();
        $items = Influencer::get();
        $dataInfluencers = [];
        $influencers = $request->influencers;
        $exportColumn = '';

        if($influencers || $request->categories) {
            if($request->filter === "true") {
                $influencers = array_unique(InfluencerCategory::whereIn('category_id', $request->categories)->get()->pluck('influencer_id')->toArray());
            }

            $getInfluencerName = Influencer::whereIn('id', $influencers);
            if($request->export_column) {
                $exportColumn = explode(',', $request->export_column);
                $getInfluencerName = Influencer::whereIn('id', $influencers);
            }
            if($request->filter === "true") {
                if($request->followers) {
                    $getInfluencerName->where('followers', '>=', $request->followers);
                }

                if($request->engagement_rate) {
                    $getInfluencerName->where('engagement_rate', '>=', $request->engagement_rate);
                }
            }
            $getInfluencerName = $getInfluencerName->get();

            // foreach ($getInfluencerName as $key => $value) {
            //     $result = null;
            //     $client = new Client([
            //         'base_uri' => $this->endpoint
            //     ]);

            //     try {
            //         $getResult = $client->request(
            //             'GET',
            //             'scrape',
            //             [
            //                 'headers' => [
            //                     'Accept'        => 'application/json',
            //                     'Content-Type' => 'application/json',
            //                 ],
            //                 'query' => [
            //                     'username' => $value->ig_name
            //                 ]
            //             ]
            //         );

            //         $result = json_decode($getResult->getBody()->getContents());
            //         $value->engagement_detail = $result;

            //         $data = [];
            //         $data['engagement_rate'] = $result->engagement;
            //         $data['engagement_rate_latest'] = Carbon::now();
            //         Influencer::where('id', $value->id)->update($data);
            //     } catch (\Throwable $th) {
            //         Log::info($th);
            //     }
            // }

            $dataInfluencers = $getInfluencerName;

            if ($request->is_export === 'true') {
                return Excel::download(new InfluencerExport($dataInfluencers, $exportColumn), 'influencerdata_' . Carbon::now()->format('d_F_Y') . '.xlsx');
            }
        }

        $arrView = [
            'categories' => $categories,
            'data_influencers' => $dataInfluencers,
            'items' => $items
        ];

        return view('pages.influencers.engagement-rate', $arrView);
    }
}
