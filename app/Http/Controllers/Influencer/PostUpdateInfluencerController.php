<?php

namespace App\Http\Controllers\Influencer;

use App\Http\Controllers\Controller;
use App\Http\Models\Influencer;
use App\Http\Models\InfluencerCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostUpdateInfluencerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $id)
    {
        $item = Influencer::find($id);
        if(!$item) {
            return redirect(url()->previous())->with('failed', 'Data not found');
        }

        try {
            DB::beginTransaction();

            $influencer = [];
            $influencer['phone_number'] = $request->phone_number;
            $influencer['email'] = $request->email;
            $influencer['ig_name'] = $request->ig_name;
            $influencer['ig_photo'] = str_replace(',', '', $request->ig_photo);
            $influencer['ig_photo_carousel'] = str_replace(',', '', $request->ig_photo_carousel);
            $influencer['ig_story'] = str_replace(',', '', $request->ig_story);
            $influencer['ig_video'] = str_replace(',', '', $request->ig_video);
            $influencer['event_attendance'] = str_replace(',', '', $request->event_attendance);
            $influencer['youtube_channel'] = $request->youtube_channel;
            $influencer['youtube_video'] = str_replace(',', '', $request->youtube_video);
            $influencer['followers'] = $request->followers;
            $influencer['engagement_rate'] = $request->engagement_rate;
            $influencer['engagement_rate_latest'] = Carbon::now();
            $influencer['notes'] = $request->notes;
            $influencer['created_by'] = Auth::id();
            Influencer::where('id', $id)->update($influencer);

            foreach ($request->categories as $key => $ctgr) {
                InfluencerCategory::where('influencer_id', $id)->delete();

                InfluencerCategory::create([
                    'category_id' => $ctgr,
                    'influencer_id' => $id
                ]);
            }
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return redirect('influencers')->withInput()->with('failed', 'Please check log');
        }

        DB::commit();
        return redirect('influencers')->with('success', 'Data updated');
    }
}
