<?php

namespace App\Http\Controllers\Influencer;

use App\Http\Controllers\Controller;
use App\Http\Models\Influencer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetInfluencerUpdateEngagementRateController extends Controller
{
    private $endpoint = 'http://34.101.251.172:5000/';
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request, $id)
    {
        $result = [];
        $result['code'] = 400;
        $result['status'] = "error";
        $result['data'] = [];

        $item = Influencer::find($id);
        $dataInfluencers = [];

        if(!$item) {
            $result['code'] = 413;
            $result['message'] = "need-login";
            return $result;
        }

        $client = new Client([
            'base_uri' => $this->endpoint
        ]);

        try {
            DB::beginTransaction();

            $getResult = $client->request(
                'GET',
                'scrape',
                [
                    'headers' => [
                        'Accept'        => 'application/json',
                        'Content-Type' => 'application/json',
                    ],
                    'query' => [
                        'username' => $item->ig_name
                    ]
                ]
            );

            $resultEngagement = json_decode($getResult->getBody()->getContents());

            $data = [];
            $data['engagement_rate'] = $resultEngagement->engagement;
            $data['engagement_rate_latest'] = Carbon::now();
            Influencer::where('id', $id)->update($data);

            $result['data'] = $data;
            $result['data']['engagement_rate'] = number_format($result['data']['engagement_rate'], 2, '.', '.');
            $result['data']['engagement_rate_latest'] = $result['data']['engagement_rate_latest']->format('d F Y, H:i');
        } catch (\Throwable $th) {
            Log::info($th);
            DB::rollback();
        }

        DB::commit();

        $result['code'] = 200;
        $result['status'] = "success";
        return $result;
        
    }
}
