<?php

namespace App\Http\Controllers\Influencer;

use App\Http\Controllers\Controller;
use App\Http\Models\Category;
use App\Http\Models\Influencer;

class GetInfluencerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke()
    {
        $categories = Category::get();
        $items = Influencer::with('categories')->orderByDesc('updated_at')->get();

        $arrView = [
            'categories' => $categories,
            'items' => $items
        ];

        return view('pages.influencers.index', $arrView);
    }
}
