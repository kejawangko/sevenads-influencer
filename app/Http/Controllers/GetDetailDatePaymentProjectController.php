<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Project;
use App\Http\Models\ProjectInfluencer;
use App\Http\Models\ProjectInfluencerPayment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class GetDetailDatePaymentProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke($date)
    {
        $result = [];
        $result['code'] = 400;
        $result['status'] = "error";
        $result['data'] = [];

        $date = Carbon::parse($date)->format('Y-m-d');

        $items = ProjectInfluencerPayment::join('project_influencers', 'project_influencer_payments.project_influencer_id', '=', 'project_influencers.id')
            ->join('projects', 'project_influencers.project_id', '=', 'projects.id')
            ->where('status', 'not paid')
            ->where('due_date', $date)
            ->where('projects.type', 'KOL')
            ->get();

        $string = '';
        $totalPayment = 0;
        foreach ($items as $key => $item) {
            $string .= "<tr><td>".($key+1)."</td><td>".Carbon::parse($item->due_date)->format('d F Y')."</td>".
                        "<td>".$item->projectInfluencers->influencer_name."</td>".
                        "<td>".
                            "Nama Pemilik Bank : ".
                            "<strong>".$item->projectInfluencers->bank_owner."</strong><br>".
                            "Nama Bank : ".
                            "<strong>".$item->projectInfluencers->bank_name."</strong><br>".
                            "Cabang Bank :".
                            "<strong>".$item->projectInfluencers->bank_branch_name."</strong><br>".
                            "Nomor Rekening : ".
                            "<strong>".$item->projectInfluencers->bank_account."</strong>".
                        "</td>".
                        "<td>".number_format($item->total, 0, '.', ',')."</td>".
                        "<td><a href='".$item->projectInfluencers->invoice."' target='_blank'>".$item->projectInfluencers->invoice."</a></td></tr>";

            $totalPayment += $item->total;
        }

        $result['data'] = [
            'data_table' => $string,
            'total_payment' => 'Rp '.number_format($totalPayment, 0, '.', ',')
        ];
        $result['code'] = 200;
        $result['status'] = "success";
        return $result;
    }
}
