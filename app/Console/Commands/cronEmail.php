<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Models\ProjectInfluencerPayment;
use App\Http\Models\User;
use App\Mail\ReminderMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class cronEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $reminderDay = Carbon::today()->addDays(2)->format('Y-m-d');
        $paymentDueDate = ProjectInfluencerPayment::where('due_date', $reminderDay)->exists();
        if ($paymentDueDate) {
            try {
                $superadminUser = User::where('role', 'superadmin')->first()->email;
                $adminUser = User::where('role', 'admin')->first()->email;
                $financeUser = User::where('role', 'finance')->first()->email;
                $sendEmail = Mail::to($financeUser)
                ->cc([$superadminUser, $adminUser, 'keshia.sevenads@gmail.com'])->send(new ReminderMail());
            } catch (\Exception $e) {
                Log::info($e);
            }
        }
    }
}
