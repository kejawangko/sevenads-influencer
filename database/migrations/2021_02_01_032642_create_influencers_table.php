<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('ig_link')->unique();
            $table->string('ig_name')->unique();
            $table->unsignedBigInteger('ig_photo');
            $table->unsignedBigInteger('ig_photo_carousel');
            $table->unsignedBigInteger('ig_story');
            $table->unsignedBigInteger('ig_video');
            $table->unsignedBigInteger('ig_reels');
            $table->unsignedBigInteger('ig_live');
            $table->unsignedBigInteger('ig_followers')->default(0);
            $table->string('youtube_channel')->nullable();
            $table->unsignedBigInteger('youtube_video')->nullable();
            $table->unsignedBigInteger('youtube_subscribers')->nullable();
            $table->string('tiktok_username')->nullable();
            $table->unsignedBigInteger('tiktok_followers')->default(0);
            $table->unsignedBigInteger('tiktok_video')->nullable();
            $table->unsignedBigInteger('event_attendance')->nullable();
            $table->float('engagement_rate', 10, 2)->nullable();
            $table->date('engagement_rate_latest')->nullable();
            $table->text('notes')->nullable();

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('influencer_categories', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('influencer_id')->nullable();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('influencer_id')->references('id')->on('influencers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencers');
    }
}
