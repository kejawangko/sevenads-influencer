<?php

namespace Database\Seeders;

use App\Http\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role'           => 'superadmin',
            'full_name'      => 'Superadmin SevenAds',
            'email'          => 'superadmin.sevenads@gmail.com',
            'phone_number'   => '89999999999',
            'dob'            => '1990-10-10',
            'password'       => Hash::make('superadmin')
        ]);

        User::create([
            'role'           => 'admin',
            'full_name'      => 'Admin Sevenads',
            'email'          => 'admin.sevenads@gmail.com',
            'phone_number'   => '87777777777',
            'dob'            => '1990-10-10',
            'password'       => Hash::make('admin')
        ]);

        User::create([
            'role'           => 'finance',
            'full_name'      => 'Finance Sevenads',
            'email'          => 'finance.sevenads@gmail.com',
            'phone_number'   => '81111111111',
            'dob'            => '1990-10-10',
            'password'       => Hash::make('finance')
        ]);
    }
}
