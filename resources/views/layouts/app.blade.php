<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin SevenAds</title>

  <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">

  <!-- Custom fonts for this template-->
  <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('assets/css/sb-admin-2.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/jquery-ui/jquery-ui.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/calendar/style.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" />

  <style>
    .select2-container--default .select2-selection--multiple {
      height: 40px;
    }

    .select2-container {
      width: 75% !important;
    }

    * {
      font-family: 'Open Sans', sans-serif;
    }
  </style>

</head>

<body @guest class="bg-gradient-primary" @else id="page-top" @endif>

  @yield('content')

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.js') }}"></script>
  <script href="{{ asset('assets/vendor/calendar/app.js') }}"></script>
  <script src="{{ asset('assets/js/select2.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('assets/js/sb-admin-2.js') }}"></script>
  <script>
    //function number format
    function convertToRupiah(angka) {
        var rupiah = '';        
        var angkarev = angka.toString().split("").reverse().join("");
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
        return rupiah.split('',rupiah.length-1).reverse().join('');
    }
    function convertToAngka(rupiah){
        if(rupiah == ""){
            return "";
        }
        else{
            return parseInt(rupiah.replace(/,*&[a-z][A-Z]|[^0-9]/g, ''), 10);
        }
    }

    function onlyNumeric(value) {
      if(value == ""){
            return "";
        }
        else{
            return value.replace(/,*&[a-z][A-Z]|[^0-9]/g, '');
        }
    }
    //end of number format

    // Call the dataTables jQuery plugin
    $(document).ready(function() {
      $('.dataTable').DataTable({
        responsive: true,
          language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
          },
          order : [],
          "bStateSave": true,
          "fnStateSave": function (oSettings, oData) {
              localStorage.setItem('offersDataTables', JSON.stringify(oData));
          },
          "fnStateLoad": function (oSettings) {
              return JSON.parse(localStorage.getItem('offersDataTables'));
          }
      });
      $('.js-example-basic-multiple').select2();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  </script>
  @yield('script')
</body>

</html>
