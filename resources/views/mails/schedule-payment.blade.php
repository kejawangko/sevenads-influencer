@extends('layouts.mails.app')
@section('email-title', 'Schedule Payment')
@section('content')
    <p style="font-size:18px;margin: 0 0 10px;">
        Dear Admin and Finance<br>
        New Project Created : {{ $projectName }}<br>
        <a href="{{ url('projects/'.$projectId.'/detail') }}">click here</a> to see the detail
	</p>
	<br><br>
@stop