@extends('layouts.mails.app')
@section('email-title', 'Payment Reminder Alert')
@section('content')
    <style>
        a.external[target="_blank"]::after {
            content: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAQElEQVR42qXKwQkAIAxDUUdxtO6/RBQkQZvSi8I/pL4BoGw/XPkh4XigPmsUgh0626AjRsgxHTkUThsG2T/sIlzdTsp52kSS1wAAAABJRU5ErkJggg==);
            margin: 0px 3px 0px 5px;
        }
    </style>
    <p style="font-size:18px;margin: 0 0 10px;">
        Hi, {{ $name }}<br>
        Welcome to SevenAds, we are really looking forward to work together.<br>
        Here's your password to login to <a href="http://platformkol.grownow.asia/" class="external" target="_blank">Platform SevenAds</a><br>
        password : <strong>{{ $password }}</strong><br>
        Good Luck!
	</p>
	<br><br>
@stop