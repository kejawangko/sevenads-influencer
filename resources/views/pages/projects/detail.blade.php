@extends('layouts.app')

@section('content')
<style>
    .form-action, .btn-edit {
      display: none;
    }
    .form-action.active, .btn-edit.active {
      display: flex;
    }

    #full-payment, #custom-payment {
        display: none;
    }

    #full-payment.active, #custom-payment.active {
        display: block;
    }

    .section {
        display: none;
    }

    .col-payment.bg-done {
        background: green !important;
    }
    .col-payment.bg-primary {
        background: lightblue !important;
        color: black;
    }

    .col-payment {
        padding: 15px 10px;
        margin: 0px 5px;
        border-radius: 20px;
        color: white;
    }
</style>
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">{{ $item->name }}</h1>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-10">
                        <h1 class="h3 mb-0 text-gray-800">General Information</h1>
                    </div>
                    <div class="col-lg-2">
                        <a href="#" class="btn btn-warning btn-edit active float-right"><i class="fas fa-edit"></i> Edit</a>
                    </div>
                </div>
                <div class="row py-3 bg-white mb-4">
                    <div class="col-lg-12 mb-4">
                        <form id="general-info-form" action="{{ url('projects/'.strtolower(str_replace(' ', '-', $item->type)).'/'.$item->id.'/update') }}" method="post">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control" value="{{ $item->name }}" readonly>
                                </div>
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <label for="budget">Budget</label>
                                    <input type="text" name="budget" class="form-control payment" value="{{ number_format($item->budget, 0, '.', ',') }}" readonly>
                                </div>
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <label for="budget">Profit</label>
                                    <input type="text" value="{{ number_format(($item->budget - $total_outcome), 0, '.', ',') }}" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <label for="start_date">Start Date</label>
                                    <input type="date" name="start_date" class="form-control" value="{{ date('Y-m-d', strtotime($item->start_date)) }}" readonly>
                                </div>
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <label for="estimate_finish_date">Estimate Finish Date</label>
                                    <input type="date" name="estimate_finish_date" class="form-control" value="{{ date('Y-m-d', strtotime($item->estimate_finish_date)) }}" readonly>
                                </div>
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <label for="start_date">Total Payout</label>
                                    <input type="text" class="form-control" value="{{ number_format($total_payout, 0, '.', ',') }}" disabled>
                                </div>
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <label for="estimate_finish_date">Total Pending Payment</label>
                                    <input type="text" class="form-control" value="{{ number_format($total_pending_payment, 0, '.', ',') }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12 mb-3 mb-sm-0">
                                    <label for="notes">Notes</label>
                                    <textarea rows="5" name="notes" id="notes" class="form-control" readonly>{{ $item->notes }}</textarea>
                                </div>
                            </div>
                            <div class="form-group form-action row mt-4">
                                <div class="col-sm-4 mb-3 mb-sm-0">
                                    <button class="btn btn-block btn-primary">
                                        Submit
                                    </button>
                                </div>
                                <div class="col-sm-4 mb-3 mb-sm-0">
                                    <a href="#" class="btn btn-block btn-secondary btn-cancel">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row py-3 bg-white">
                    <div class="col-lg-8">
                        <h1 class="h3 mb-0 text-gray-800">Influencer Information</h1>
                    </div>
                    <div class="col-lg-2">
                        <a href="{{ url('projects/'.$type.'/send-email-payment-schedules/' . $item->id) }}" class="btn btn-info float-right"><i class="fas fa-paper-plane"></i> Broadcast</a>
                    </div>
                    <div class="col-lg-2">
                        <a href="#" class="btn btn-primary btn-add float-right" data-toggle="modal" data-target="#addInfModal"><i class="fas fa-plus"></i> Add Influencer</a>
                    </div>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th width="10%">Invoice Number</th>
                                        <th width="15%">Influencer Name</th>
                                        <th>Payment Schedule</th>
                                        <th width="5%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($item->projectInfluencers()->get() as $key => $proInf)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>
                                                {{ $proInf->invoice_number ?? '-' }}
                                            </td>
                                            <td>
                                                {{ $proInf->influencer_name }}
                                            </td>
                                            <td>
                                                <div class="row">
                                                    @foreach ($proInf->projectPayments()->get() as $idx => $payment)
                                                        @php
                                                            $btnInfo = 'primary';
                                                            if($payment->status === 'paid') {
                                                                $btnInfo = 'done';
                                                            } else if(\Carbon\Carbon::today()->addDays(2)->format('Y-m-d') === \Carbon\Carbon::parse($payment->due_date)->format('Y-m-d')) {
                                                                $btnInfo = 'danger';
                                                            }
                                                        @endphp
                                                        <div class="col-md-5 text-center">
                                                            <div class="col-payment bg-{{ $btnInfo }} mb-2">
                                                                Payment #{{ $idx+1 }} = <strong>Rp. {{ number_format($payment->total, 0, '.', ',') }}</strong><br>
                                                                <strong>{{ date('d F Y', strtotime($payment->due_date)) }}</strong>
                                                            </div>
                                                            @if($payment->status === 'not paid')
                                                                <a href="#" class="btn-edit-payment" data-target="#editInfModal" 
                                                                    data-url="{{ route('update-schedule-influencer-payment', ['type' => strtolower(str_replace(' ', '-', $proInf->projects->type)), 'id' => $payment->id]) }}"
                                                                    data-nameinf="{{ $proInf->influencer_name }}"
                                                                    data-bankname="{{ $proInf->bank_name }}"
                                                                    data-bankbranchname="{{ $proInf->bank_branch_name }}"
                                                                    data-bankaccount="{{ $proInf->bank_account }}"
                                                                    data-bankowner="{{ $proInf->bank_owner }}"
                                                                    data-invoice="{{ $proInf->invoice }}"
                                                                    data-invoicenumber="{{ $proInf->invoice_number }}"
                                                                    data-payment="{{ $payment->total }}"
                                                                    data-duedate="{{ date('Y-m-d', strtotime($payment->due_date)) }}">update payment</a>
                                                                &nbsp;|&nbsp;
                                                                <a href="#" class="btn-conf-payment" data-target="#paymentConfModal" 
                                                                    data-url="{{ route('update-influencer-payment', ['type' => strtolower(str_replace(' ', '-', $proInf->projects->type)), 'id' => $payment->id]) }}"
                                                                    data-nameinf="{{ $proInf->influencer_name }}"
                                                                    data-payment="{{ $payment->total }}"
                                                                    data-duedate="{{ date('d F Y', strtotime($payment->due_date)) }}">paid</a>
                                                                &nbsp;|&nbsp;
                                                                <a href="#" class="btn-conf-del-payment" data-target="#paymentDelConfModal" 
                                                                    data-url="{{ route('delete-influencer-payment', ['type' => strtolower(str_replace(' ', '-', $proInf->projects->type)), 'id' => $payment->id]) }}"
                                                                    data-nameinf="{{ $proInf->influencer_name }}"
                                                                    data-payment="{{ $payment->total }}"
                                                                    data-duedate="{{ date('d F Y', strtotime($payment->due_date)) }}"
                                                                    style="color: red">delete</a>
                                                            @endif
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </td>
                                            <td><button class="btn btn-sm btn-primary btn-add-new-payment-schedule" data-toggle="modal" data-target="#addNewPayment" data-id="{{ $proInf->id }}" data-name="{{ $proInf->influencer_name }}"><i class="fas fa-plus"></i> Add New Payment</button></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<div class="modal fade" id="addInfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Influencer</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{ url('projects/'.$type.'/'.$item->id.'/influencer/store') }}" id="form-add-new-influencer" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="name">Influencer Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Influencer name" value="{{ old('name') }}" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="bank_name">Nama Bank</label>
                            <input type="text" name="bank_name" class="form-control" placeholder="Nama Bank" value="{{ old('bank_name') }}" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="bank_branch_name">Cabang Bank</label>
                            <input type="text" name="bank_branch_name" class="form-control" placeholder="Cabang Bank" value="{{ old('bank_branch_name') }}" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="bank_account">Nomor Rekening</label>
                            <input type="text" name="bank_account" class="form-control" placeholder="Nomor Rekening" value="{{ old('bank_account') }}" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="bank_owner">Nama Pemilik Rekening</label>
                            <input type="text" name="bank_owner" class="form-control" placeholder="Nama Pemilik Rekening" value="{{ old('bank_owner') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="invoice_number">Invoice Number (Ex. SAxxx)</label>
                            <input type="text" name="invoice_number" id="invoice_number" class="form-control" placeholder="Invoice Number" value="{{ old('invoice_number') }}" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="invoice">Invoice</label>
                            <input type="text" name="invoice" id="invoice" class="form-control" placeholder="Invoice" value="{{ old('invoice') }}" required autocomplete="off">
                        </div>
                    </div>
                    <div id="custom-payment" class="active">
                        <label>Payment</label>
                        <a href="#" id="btn-add-input" class="float-right">add</a>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="text" name="payment[]" class="form-control payment payment-influencer" value="{{ old('payment[]') }}">
                                </div>
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" name="due_date[]" class="form-control due_date datepicker" value="{{ old('due_date[]') }}" autocomplete='off'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="button" id="btn-add-new-influencer">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editInfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Payment Schedule</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="edit-payment-schedule-form" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="name">Influencer Name</label>
                            <input type="text" name="name" id="influencer-name" class="form-control" placeholder="Influencer name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="bank_name">Nama Bank</label>
                            <input type="text" name="bank_name" id="influencer-bank-name" class="form-control" placeholder="Nama Bank" value="{{ old('bank_name') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="bank_branch_name">Cabang Bank</label>
                            <input type="text" name="bank_branch_name" id="influencer-bank-branch-name" class="form-control" placeholder="Cabang Bank" value="{{ old('bank_branch_name') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="bank_account">Nomor Rekening</label>
                            <input type="text" name="bank_account" id="influencer-bank-account" class="form-control" placeholder="Nomor Rekening" value="{{ old('bank_account') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="bank_owner">Nama Pemilik Rekening</label>
                            <input type="text" name="bank_owner" id="influencer-bank-owner" class="form-control" placeholder="Nama Pemilik Rekening" value="{{ old('bank_owner') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="invoice_number">Invoice Number (Ex. SAxxx)</label>
                            <input type="text" name="invoice_number" id="influencer-invoice-number" class="form-control" placeholder="Invoice Number" value="{{ old('invoice_nummber') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <label for="invoice">Invoice</label>
                            <input type="text" name="invoice" id="influencer-invoice" class="form-control" placeholder="Invoice" value="{{ old('invoice') }}">
                        </div>
                    </div>
                    <label>Payment</label>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp.</span>
                                </div>
                                <input type="text" name="payment" id="influencer-payment" class="form-control payment" value="">
                            </div>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" name="due_date" id="influencer-due-date" class="form-control due_date datepicker" value="" autocomplete='off'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="addNewPayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Payment Schedule for : <strong><span id="influencer-name-add-new-payment-schedule"></span></strong></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{ url('projects/'.strtolower(str_replace(' ', '-', $item->type)).'/'.$item->id.'/influencer/store') }}" id="form-add-new-payment-additional" method="post">
                @csrf
                <input type="hidden" name="id_project_influencer" id="id-project-influencer">
                <div class="modal-body">
                    <div id="custom-paymentschedule-additional" class="active">
                        <label>Payment</label>
                        <a href="#" id="btn-add-input-paymentschedule" class="float-right">add</a>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="text" name="payment[]" class="form-control payment payment-influencer" value="{{ old('payment[]') }}">
                                </div>
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" name="due_date[]" class="form-control due_date datepicker" value="{{ old('due_date[]') }}" autocomplete='off'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit" id="btn-add-new-influencer">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="paymentDelConfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Payment Confirmation</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="form-delete-payment" action="#" method="get">
                @csrf
                <div class="modal-body">
                    <h4>Payment Detail</h4>
                    <span id="inf-name"></span><br>
                    Rp. <span id="inf-payment"></span><br>
                    <span id="inf-duedate"></span><br><br>

                    <strong>Are sure ? Once you update there's no turning back .</strong>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button id="btn-conf-del-payment-modal" class="btn btn-primary" type="submit">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="paymentConfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Payment Confirmation</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="#" method="post">
                @csrf
                <div class="modal-body">
                    <h4>Payment Detail</h4>
                    <span id="inf-name"></span><br>
                    Rp. <span id="inf-payment"></span><br>
                    <span id="inf-duedate"></span><br><br>
                    <div class="form-group">
                        <label for="payment_number">Payment Number (nomor resi dari Jurnal)</label>
                        <input type="text" class="form-control" name="payment_number">
                    </div>

                    <strong>Are sure ? Once you update there's no turning back .</strong>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button id="btn-conf-payment-modal" class="btn btn-primary" type="submit">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    const picker = document.getElementsByClassName('due_date');
    for (var i = 0 ; i < picker.length; i++) {
        picker[i].addEventListener('input', function(e) {
            var day = new Date(this.value).getUTCDay();
            console.log(day);
            console.log(this.value);
            if(![5,0].includes(day)){
                e.preventDefault();
                this.value = '';
                alert('only Friday allowed');
            }
        });
    }

    $('body').on('keyup', '.payment', function() {
        var value = convertToAngka($(this).val());

        $(this).val(convertToRupiah(value));
    });

    $('.btn-edit').click(function (e) {
        e.preventDefault();

        $('#general-info-form :input').prop('readonly', false);
        $('.form-action').addClass('active');
        $(this).removeClass('active');
    })

    $('.btn-cancel').click(function (e) {
        e.preventDefault();

        $('#general-info-form :input').prop('readonly', true);
        $('.form-action').removeClass('active');
        $('.btn-edit').addClass('active');
    })


    $(document).ready(function() {
        $('body').on('click', '#btn-add-input', function(e) {
            e.preventDefault();

            var maxAdditional = 10;

            var inputLength = $('input[name^=payment]').length;
            console.log(inputLength);
            if(+inputLength === +maxAdditional) {
                alert('cannot add another name');
                return false;
            }
            
            var inputTarget = $('#custom-payment');
            var additionalInput = "<div class='form-group row'><div class='col-sm-6 mb-3 mb-sm-0'>"+
                    "<div class='input-group'>"+
                        "<div class='input-group-prepend'>"+
                            "<span class='input-group-text'>Rp.</span>"+
                        "</div>"+
                        "<input type='text' name='payment[]' class='form-control payment'>"+
                    "</div>"+
                "</div>"+
                "<div class='col-sm-6 mb-3 mb-sm-0'>"+
                    "<input type='text' name='due_date[]' class='form-control due_date datepicker' autocomplete='off'>"+
                "</div>"+
                "<div class='col-sm-12 mb-3 mb-sm-0'>"+
                    "<a href='#' class='btn-remove-input float-right'>remove</a>"+
                "</div>"+
            "</div>";

            inputTarget.append(additionalInput).find('.datepicker').datepicker({dateFormat: 'dd-mm-yy',
                minDate: 1,
                beforeShowDay: enableFirday});
        });

        $('body').on('click', '#btn-add-input-paymentschedule', function(e) {
            e.preventDefault();

            var maxAdditional = 10;

            var inputLength = $('#form-add-new-payment-additional input[name^=payment]').length;
            console.log(inputLength);
            if(+inputLength === +maxAdditional) {
                alert('cannot add another name');
                return false;
            }
            
            var inputTarget = $('#custom-paymentschedule-additional');
            var additionalInput = "<div class='form-group row'><div class='col-sm-6 mb-3 mb-sm-0'>"+
                    "<div class='input-group'>"+
                        "<div class='input-group-prepend'>"+
                            "<span class='input-group-text'>Rp.</span>"+
                        "</div>"+
                        "<input type='text' name='payment[]' class='form-control payment'>"+
                    "</div>"+
                "</div>"+
                "<div class='col-sm-6 mb-3 mb-sm-0'>"+
                    "<input type='text' name='due_date[]' class='form-control due_date datepicker' autocomplete='off'>"+
                "</div>"+
                "<div class='col-sm-12 mb-3 mb-sm-0'>"+
                    "<a href='#' class='btn-remove-input float-right'>remove</a>"+
                "</div>"+
            "</div>";

            

            inputTarget.append(additionalInput).find('.datepicker').datepicker({dateFormat: 'dd-mm-yy',
                minDate: 1,
                beforeShowDay: enableFirday});
        });

        $('body').on('click', '.btn-remove-input', function(e) {
            e.preventDefault();

            var inputTarget = $(this).closest('div.form-group');
            inputTarget.remove();
        });

        $('.btn-edit-payment').on('click', function(e) {
            e.preventDefault();
            
            var thisElement = $(this);
            var targetModal = thisElement.attr('data-target');
            var url = thisElement.attr('data-url');
            var nameInf = thisElement.attr('data-nameinf');
            var bankName = thisElement.attr('data-bankname');
            var bankBranchName = thisElement.attr('data-bankbranchname');
            var bankAccount = thisElement.attr('data-bankaccount');
            var bankOwner = thisElement.attr('data-bankowner');
            var invoice = thisElement.attr('data-invoice');
            var invoiceNumber = thisElement.attr('data-invoicenumber');
            var paymentSched = convertToRupiah(thisElement.attr('data-payment'));
            var dueDate = thisElement.attr('data-duedate');


            $(targetModal).find('#influencer-name').val(nameInf)
            $(targetModal).find('#influencer-bank-name').val(bankName)
            $(targetModal).find('#influencer-bank-branch-name').val(bankBranchName)
            $(targetModal).find('#influencer-bank-account').val(bankAccount)
            $(targetModal).find('#influencer-bank-owner').val(bankOwner)
            $(targetModal).find('#influencer-invoice').val(invoice)
            $(targetModal).find('#influencer-invoice-number').val(invoiceNumber)
            $(targetModal).find('#influencer-payment').val(paymentSched)
            $(targetModal).find('#influencer-due-date').val(dueDate)
            $(targetModal).find('#edit-payment-schedule-form').attr('action', url)
            $(targetModal).modal('show');
        })

        $('.btn-conf-payment').on('click', function(e) {
            e.preventDefault();

            var thisElement = $(this);
            var targetModal = thisElement.attr('data-target');
            var url = thisElement.attr('data-url');
            var nameInf = thisElement.attr('data-nameinf');
            var paymentSched = convertToRupiah(thisElement.attr('data-payment'));
            var dueDate = thisElement.attr('data-duedate');

            $(targetModal).find('#inf-name').text(nameInf)
            $(targetModal).find('#inf-payment').text(paymentSched)
            $(targetModal).find('#inf-duedate').text(dueDate)
            $(targetModal).find('form').attr('action', url)
            $(targetModal).modal('show');
        })

        $(document).on('click', '.btn-add-new-payment-schedule', function(e) {
            e.preventDefault();

            var influencerName = $(this).attr('data-name');
            var id = $(this).attr('data-id');

            $('#influencer-name-add-new-payment-schedule').text(influencerName)
            $('#id-project-influencer').val(id);
        })

        $(document).on('click', '.btn-conf-del-payment', function(e) {
            e.preventDefault();

            var thisElement = $(this);
            var targetModal = thisElement.attr('data-target');
            var url = thisElement.attr('data-url');
            var nameInf = thisElement.attr('data-nameinf');
            var paymentSched = convertToRupiah(thisElement.attr('data-payment'));
            var dueDate = thisElement.attr('data-duedate');

            $(targetModal).find('#inf-name').text(nameInf)
            $(targetModal).find('#inf-payment').text(paymentSched)
            $(targetModal).find('#inf-duedate').text(dueDate)
            $(targetModal).find('form').attr('action', url)
            $(targetModal).modal('show');
        })

        $('#btn-add-new-influencer').on('click', function(e) {
            e.preventDefault();

            paymentMinimum = 0;
            $('.form-control.payment.payment-influencer').each(function(){
                paymentMinimum += convertToAngka($(this).val());
            });

            if(paymentMinimum >= 1000000 && ($('#form-add-new-influencer #invoice').val() === '' || $('#form-add-new-influencer #invoice').val() === null)) {
                alert('Please insert invoice link');
                return false;
            }

            $('#form-add-new-influencer').submit();
        });

        $('body').on("keyup", "input[name=bank_account]", function() {
            var value = onlyNumeric($(this).val());
            if(isNaN(value)) {
                value = "";
            }
            $(this).val(value);
        });
    });

    function enableFirday(d) { 
        var disabledDate= new Date(2010,11,31);

        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();

        var dDate = new Date(curr_year, curr_month, curr_date);
        return [(d.getDay() == 5 && dDate.toString() != disabledDate.toString()), ''];
    }

    $('.datepicker').datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: 1,
        beforeShowDay: enableFirday
    });
</script>
@endsection