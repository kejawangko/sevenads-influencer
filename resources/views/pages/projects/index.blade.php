@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Projects</h1>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <form action="{{ url('projects/'.$type.'/store') }}" method="post">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Name</span>
                                </div>
                                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Budget</span>
                                </div>
                                <input type="text" name="budget" id="budget" class="form-control payment" value="{{ old('budget') }}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Start Date</span>
                                </div>
                                <input type="date" name="start_date" id="start_date" class="form-control" value="{{ old('start_date') }}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Estimate Finish Date</span>
                                </div>
                                <input type="date" name="estimate_finish_date" id="estimate_finish_date" class="form-control" value="{{ old('estimate_finish_date') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Project</th>
                                        <th>Budget Detail</th>
                                        <th>Total Profit</th>
                                        <th>Total Payout</th>
                                        <th>Total Pending Payment</th>
                                        <th>Start Date</th>
                                        <th>Finish Date</th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($items as $key => $item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>
                                                {{ $item->name }}
                                            </td>
                                            <td>
                                                {{ number_format($item->budget, 0, '.', ',') }}
                                            </td>
                                            <td>
                                                {{ number_format($item->profit, 0, '.', ',') }}
                                            </td>
                                            <td>
                                                {{ number_format($item->payout, 0, '.', ',') }}
                                            </td>
                                            <td>
                                                {{ number_format($item->pending_payment, 0, '.', ',') }}
                                            </td>
                                            <td>{{ $item->start_date ? date('d F Y', strtotime($item->start_date)) : '-' }}</td>
                                            <td>{{ $item->estimate_finish_date ? date('d F Y', strtotime($item->estimate_finish_date)) : '-' }}</td>
                                            <td class="text-center">
                                                <a href="{{ url('projects/'.strtolower(str_replace(' ', '-', $item->type)).'/'.$item->id.'/detail') }}" class="btn btn-primary" style="color: white">
                                                    <span class="fas fa-search" title="detail"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@endsection

@section('script')
<script>
    $('body').on('keyup', '.payment', function() {
        var value = convertToAngka($(this).val());

        $(this).val(convertToRupiah(value));
    });
</script>
@endsection