@extends('layouts.app')

@section('content')
<style>
    .loader {
        border: 2px solid #f3f3f3;
        border-radius: 50%;
        border-top: 2px solid black;
        width: 20px;
        height: 20px;
        margin-left: 10px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
        display: none;
    }

    th {
        font-size: 14px;
    }
    td {
        font-size: 14px;
    }
</style>


<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Projects</h1>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <a href="{{ url('influencers/engagement-rate') }}" class="btn btn-primary float-right ml-3"><i class="fas fa-plus"></i> Pitching Needed</a>
                        <a href="#" class="btn btn-primary float-right btn-add-influencer" data-toggle="modal" data-target="#addInfluencerModal"><i class="fas fa-plus"></i> Add Influencer</a>
                        <form id="import-influencer" method="POST" action="{{ url('influencers/import') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="file" style="display:none;" name="import_influencer" id="importFile" multiple />
                            <button class="btn btn-info float-right mr-3" type="button" id="choose-file"><i class="fas fa-file-upload"></i> Import Influencer<div class="loader"></div></button>
                        </form>
                    </div>
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Ig username</th>
                                        <th>Engagement Rate</th>
                                        <th>Total Followers</th>
                                        <th width="20%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($items as $key => $item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td id="datacategory-{{ $key+1 }}" data-categoryid="{{ json_encode($item->categories->pluck('category_id')->toArray()) }}">
                                                @foreach ($item->categories as $ctgry)
                                                    {{ $ctgry->categories->name.', ' }}
                                                @endforeach
                                            </td>
                                            <td id="dataigname-{{ $key+1 }}" data-igname="{{ $item->ig_name}}">
                                                <a href="https://instagram.com/{{ $item->ig_name}}" target="_blank">{{ $item->ig_name}}</a>
                                            </td>
                                            <td>
                                                <span id="dataengagementrate-{{ $key+1 }}">{{ number_format($item->engagement_rate, 2, '.', '.') }}%</span><br>
                                                <span id="dataengagementratelatest-{{ $key+1 }}">Last Update : {{ $item->engagement_rate_latest === null ? '-' : date('d F Y, H:i', strtotime($item->engagement_rate_latest)) }}</span>

                                                <div class="d-none">
                                                    <span id="dataphonenumber-{{ $key+1 }}">{{ $item->phone_number }}</span>
                                                    <span id="dataemail-{{ $key+1 }}">{{ $item->email }}</span>
                                                </div>
                                            </td>
                                            <td id="datafollower-{{ $key+1 }}">
                                                {{ number_format($item->followers, 0, '.', '.') }}
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-primary btn-detail" style="color: white"
                                                    data-id="{{ $key+1 }}"
                                                    data-igphoto="{{ $item->ig_photo }}"
                                                    data-igphotocarousel="{{ $item->ig_photo_carousel }}"
                                                    data-igstory="{{ $item->ig_story }}"
                                                    data-igvideo="{{ $item->ig_video }}"
                                                    data-eventattendance="{{ $item->event_attendance}}"
                                                    data-youtubechannel="{{ $item->youtube_channel }}"
                                                    data-youtubevideo="{{ $item->youtube_video }}">
                                                    Detail
                                                </a>
                                                <a href="#" class="btn btn-warning btn-edit" style="color: white"
                                                    data-id="{{ $key+1 }}"
                                                    data-igphoto="{{ $item->ig_photo }}"
                                                    data-igphotocarousel="{{ $item->ig_photo_carousel }}"
                                                    data-igstory="{{ $item->ig_story }}"
                                                    data-igvideo="{{ $item->ig_video }}"
                                                    data-eventattendance="{{ $item->event_attendance}}"
                                                    data-youtubechannel="{{ $item->youtube_channel }}"
                                                    data-youtubevideo="{{ $item->youtube_video }}"
                                                    data-url="{{ url('influencers/update/'.$item->id) }}">
                                                    Edit
                                                </a><br>
                                                <a href="#" class="btn btn-info btn-sync-engagementrate mt-1" style="color: white"
                                                    data-id="{{ $key+1 }}">
                                                    Refresh ER
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<style>
    #detailInfModal td {
        border: 1px solid black;
        padding: 10px 15px;
    }

    #detailInfModal td:first-child {
        width: 45%;
    }
    .mb-30 {
        margin-bottom: 30px;
    }
</style>

<div class="modal fade" id="detailInfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Influencer</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Engagement Rate : <strong><span id="detail-inf-engagement-rate"></span></strong><br>
                    Engagement Rate Last Update : <strong><span id="detail-inf-engagement-rate-latest"></span></strong>
                </p>
                <table class="mb-30" width="100%" style="word-break: break-word">
                    <tr>
                        <td>Category</td>
                        <td id="detail-inf-category"></td>
                    </tr>
                    <tr>
                        <td>Phone Number</td>
                        <td id="detail-inf-phonenumber"></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td id="detail-inf-email"></td>
                    </tr>
                    <tr>
                        <td>Ig Name</td>
                        <td id="detail-inf-igname"></td>
                    </tr>
                    <tr>
                        <td>Ig Photo</td>
                        <td id="detail-inf-igphoto"></td>
                    </tr>
                    <tr>
                        <td>Ig Photo Carousel</td>
                        <td id="detail-inf-igphotocarousel"></td>
                    </tr>
                    <tr>
                        <td>Ig Story</td>
                        <td id="detail-inf-igstory"></td>
                    </tr>
                    <tr>
                        <td>Ig Video</td>
                        <td id="detail-inf-igvideo"></td>
                    </tr>
                    <tr>
                        <td>Event Attendance</td>
                        <td id="detail-inf-envetattendance"></td>
                    </tr>
                    <tr>
                        <td>Youtube Channel</td>
                        <td id="detail-inf-youtubechannel">
                            <a href="" target="_blank"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>Youtube Video</td>
                        <td id="detail-inf-youtubevideo"></td>
                    </tr>
                </table>
                <p>
                    <strong>Notes :</strong>
                    <span id="detail-inf-followers"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addInfluencerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Influencer</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{ url('influencers/store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Category</label>
                            <select class="js-example-basic-multiple" name="categories[]" multiple="multiple" width="100%">
                                <option disabled>Select category</option>
                                @foreach ($categories as $ctgry)
                                    <option value="{{ $ctgry->id }}">{{ $ctgry->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Phone Number</label>
                            <input type="text" name="phone_number" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Name</label>
                            <input type="text" name="ig_name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Photo (IDR)</label>
                            <input type="text" name="ig_photo" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Photo Carousel (IDR)</label>
                            <input type="text" name="ig_photo_carousel" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Story (IDR)</label>
                            <input type="text" name="ig_story" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Video (IDR)</label>
                            <input type="text" name="ig_video" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Event Attendance (IDR)</label>
                            <input type="text" name="event_attendance" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Youtube Channel (link)</label>
                            <input type="text" name="youtube_channel" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Youtube Video (IDR)</label>
                            <input type="text" name="youtube_video" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Followers</label>
                            <input type="number" name="followers" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Engagement Rate (%)</label>
                            <input type="number" step="0.1" name="engagement_rate" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Notes</label>
                            <textarea name="notes" id="notes" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Okay</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editInfluencerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Influencer</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="#" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Category</label>
                            <select class="js-example-basic-multiple" id="edit-category" name="categories[]" multiple="multiple" width="100%">
                                <option disabled>Select category</option>
                                @foreach ($categories as $ctgry)
                                    <option value="{{ $ctgry->id }}">{{ $ctgry->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Phone Number</label>
                            <input type="text" id="edit-phone_number" name="phone_number" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Email</label>
                            <input type="text" id="edit-email" name="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Name</label>
                            <input type="text" id="edit-ig_name" name="ig_name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Photo (IDR)</label>
                            <input type="text" id="edit-ig_photo" name="ig_photo" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Photo Carousel (IDR)</label>
                            <input type="text" id="edit-ig_photo_carousel" name="ig_photo_carousel" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Story (IDR)</label>
                            <input type="text" id="edit-ig_story" name="ig_story" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Ig Video (IDR)</label>
                            <input type="text" id="edit-ig_video" name="ig_video" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Event Attendance (IDR)</label>
                            <input type="text" id="edit-event_attendance" name="event_attendance" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Youtube Channel (link)</label>
                            <input type="text" id="edit-youtube_channel" name="youtube_channel" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Youtube Video (IDR)</label>
                            <input type="text" id="edit-youtube_video" name="youtube_video" class="form-control ammount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Followers</label>
                            <input type="number" id="edit-followers" name="followers" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Engagement Rate (%)</label>
                            <input type="number" id="edit-engagement_rate" step="0.1" name="engagement_rate" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Notes</label>
                            <textarea name="notes" id="edit-notes" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Okay</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('body').on('keyup', '.payment', function() {
        var value = convertToAngka($(this).val());

        $(this).val(convertToRupiah(value));
    });

    $('.btn-detail').on('click', function(e) {
        e.preventDefault();
        
        var closestTr = $(this).closest('tr');
        var target = $('#detailInfModal');
        var id = $(this).attr('data-id');

        var igPhoto = convertToRupiah($(this).attr('data-igphoto'));
        var igPhotoCarousel = convertToRupiah($(this).attr('data-igphotocarousel'));
        var igStory = convertToRupiah($(this).attr('data-igstory'));
        var igVideo = convertToRupiah($(this).attr('data-igvideo'));
        var eventAttendance = convertToRupiah($(this).attr('data-eventattendance'));
        var youtubeChannel = $(this).attr('data-youtubechannel');
        var youtubeVideo = convertToRupiah($(this).attr('data-youtubevideo'));

        target.find('#detail-inf-category').text(closestTr.find('#datacategory-'+id).text());
        target.find('#detail-inf-phonenumber').html(closestTr.find('#dataphonenumber-'+id).html());
        target.find('#detail-inf-email').html(closestTr.find('#dataemail-'+id).html());
        target.find('#detail-inf-igname').html(closestTr.find('#dataigname-'+id).html());
        target.find('#detail-inf-igphoto').text('Rp. '+igPhoto);
        target.find('#detail-inf-igphotocarousel').text('Rp. '+igPhotoCarousel);
        target.find('#detail-inf-igstory').text('Rp. '+igStory);
        target.find('#detail-inf-igvideo').text('Rp. '+igVideo);
        target.find('#detail-inf-envetattendance').text('Rp. '+eventAttendance);
        target.find('#detail-inf-youtubechannel a').attr('href', youtubeChannel).html(youtubeChannel);
        target.find('#detail-inf-youtubevideo').text('Rp. '+youtubeVideo);
        target.find('#detail-inf-followers').text(closestTr.find('#datafollower-'+id).text());
        target.find('#detail-inf-engagement-rate').text(closestTr.find('#dataengagementrate-'+id).text());
        target.find('#detail-inf-engagement-rate-latest').text(closestTr.find('#dataengagementratelatest-'+id).text());

        target.modal('show');
    })

    $('.btn-edit').on('click', function(e) {
        e.preventDefault();
        
        var closestTr = $(this).closest('tr');
        var target = $('#editInfluencerModal');
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');

        var categories = JSON.parse(closestTr.find('#datacategory-'+id).attr('data-categoryid'));
        var igPhoto = convertToRupiah($(this).attr('data-igphoto'));
        var igPhotoCarousel = convertToRupiah($(this).attr('data-igphotocarousel'));
        var igStory = convertToRupiah($(this).attr('data-igstory'));
        var igVideo = convertToRupiah($(this).attr('data-igvideo'));
        var eventAttendance = convertToRupiah($(this).attr('data-eventattendance'));
        var youtubeChannel = $(this).attr('data-youtubechannel');
        var youtubeVideo = convertToRupiah($(this).attr('data-youtubevideo'));

        var followers = convertToAngka(closestTr.find('#datafollower-'+id).text());
        var engagementRate = convertToAngka(closestTr.find('#datafollower-'+id).text().replace('%', ''));

        target.find('#edit-category.js-example-basic-multiple').val(categories).change();
        target.find('#edit-phone_number').val(closestTr.find('#dataphonenumber-'+id).html());
        target.find('#edit-email').val(closestTr.find('#dataemail-'+id).html());
        target.find('#edit-ig_name').val((closestTr.find('#dataigname-'+id).attr('data-igname')));
        target.find('#edit-ig_photo').val(igPhoto);
        target.find('#edit-ig_photo_carousel').val(igPhotoCarousel);
        target.find('#edit-ig_story').val(igStory);
        target.find('#edit-ig_video').val(igVideo);
        target.find('#edit-event_attendance').val(eventAttendance);
        target.find('#edit-youtube_channel').val(youtubeChannel);
        target.find('#edit-youtube_video').val(youtubeVideo);
        target.find('#edit-followers').val(followers);
        target.find('#edit-engagement_rate').val(engagementRate);
        target.find('form').attr('action', url)

        target.modal('show');
    })

    $('.shown').modal('show');

    $('#choose-file').click(function() {
        document.getElementById('importFile').click();
    });

    $("#importFile").change(function() { 
        $('#import-influencer').submit();
        $('.loader').show();
    });


    $(".btn-sync-engagementrate").click(function(e) {
        e.preventDefault();

        console.log('aa');

        var buttonSync = $(this);
        var closestTr = $(this).closest('tr');
        var id = $(this).data('id');

        buttonSync.find('span.fa-sync').addClass('fa-spin');

        $.ajax({
            url: "{{ url('influencers/update-engagement-rate') }}/"+id,
            method: 'get',
        }).done(function (result) {
            console.log(result);
            let engagementRate = result.data.engagement_rate+'%';
            let engagementRateLatest = result.data.engagement_rate_latest;
            
            closestTr.find('#dataengagementrate-'+id).text(engagementRate);
            closestTr.find('#dataengagementratelatest-'+id).text(engagementRateLatest);

            if(buttonSync.find('span.fa-sync').hasClass('fa-spin')) {
                buttonSync.find('span.fa-sync').removeClass('fa-spin');
            }
        }).fail(function (err) {
            
        })
    })

    $('body').on('keyup', '.ammount', function() {
        var value = convertToAngka($(this).val());

        $(this).val(convertToRupiah(value));
    });
</script>
@endsection