@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Pitching Needed</h1>
                </div>
                <div class="row py-3 mb-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <form action="{{ url('influencers/engagement-rate') }}" method="get">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Influencer</span>
                                </div>
                                <select class="js-example-basic-multiple" name="influencers[]" multiple="multiple" width="100%">
                                    <option disabled>Select influencer</option>
                                    @foreach ($items as $item)
                                        <option value="{{ $item->id }}">
                                            @foreach ($item->categories as $ctgry)
                                                {{ $ctgry->categories->name.', ' }}
                                            @endforeach
                                            - {{ $item->ig_name }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Ig username</th>
                                        <th>Youtube Channel</th>
                                        <th width="30%">Notes</th>
                                        <th width="10%">Engagement Rate</th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(app('request')->input('filter') != true)
                                        @foreach ($data_influencers as $key => $item)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td id="datacategory-{{ $key+1 }}">
                                                    @foreach ($item->categories as $ctgry)
                                                        {{ $ctgry->categories->name.', ' }}
                                                    @endforeach
                                                </td>
                                                <td id="dataigname-{{ $key+1 }}">
                                                    <a href="https://instagram.com/{{ $item->ig_name}}" target="_blank">{{ $item->ig_name}}</a>
                                                </td>
                                                <td id="dataytchannel-{{ $key+1 }}">
                                                    <a href="{{ $item->youtube_channel }}" target="_blank">{{ $item->youtube_channel }}</a>
                                                </td>
                                                <td id="datanotes-{{ $key+1 }}">
                                                    {{ $item->notes }}
                                                </td>
                                                <td>
                                                    {{ number_format($item->engagement_detail->engagement, 2, ',', '.') }}%
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" class="btn btn-primary btn-detail" style="color: white"
                                                        data-id="{{ $key+1 }}"
                                                        data-igphoto="{{ $item->ig_photo }}"
                                                        data-igphotocarousel="{{ $item->ig_photo_carousel }}"
                                                        data-igstory="{{ $item->ig_story }}"
                                                        data-igvideo="{{ $item->ig_video }}"
                                                        data-eventattendance="{{ $item->event_attendance}}"
                                                        data-youtubevideo="{{ $item->youtube_video }}">
                                                        <span class="fas fa-search" title="detail"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-10 mb-3">
                        <h4 class="text-gray-800">Filter</h4>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-block btn-primary btn-export"><i class="fas fa-download"></i> Export</button>
                    </div>
                    <div class="col-lg-12">
                        <form id="form-filter" action="{{ url('influencers/engagement-rate') }}" method="get">
                            @csrf
                            <input type="hidden" name="filter" value="true">
                            <input type="hidden" name="export_column" id="export_column">
                            <input type="hidden" name="is_export" id="is_export" id="false">
                            <div class="row mb-3">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Category</span>
                                        </div>
                                        <select class="js-example-basic-multiple" name="categories[]" multiple="multiple" width="100%">
                                            <option disabled>Select category</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" @if(app('request')->input('categories') && in_array($category->id, app('request')->input('categories'))) selected @endif>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 mb-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Total Follower</span>
                                        </div>
                                        <input type="text" name="followers" id="followers" class="form-control payment" value="{{ old('followers') }}">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Engagement Rate</span>
                                        </div>
                                        <input type="number" step="0.1" name="engagement_rate" id="engagement_rate" class="form-control" value="{{ old('engagement_rate') }}">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">%</span>
                                        </div>
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-lg-12 mb-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Category</th>
                                                    <th>Ig username</th>
                                                    <th>Followers</th>
                                                    <th width="15%">Engagement Rate</th>
                                                    <th width="15%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data_influencers as $key => $item)
                                                    <tr>
                                                        <td>{{ $key+1 }}</td>
                                                        <td id="datacategory-{{ $key+1 }}">
                                                            @foreach ($item->categories as $ctgry)
                                                                {{ $ctgry->categories->name.', ' }}
                                                            @endforeach
                                                        </td>
                                                        <td id="dataigname-{{ $key+1 }}">
                                                            <a href="https://instagram.com/{{ $item->ig_name}}" target="_blank">{{ $item->ig_name}}</a>
                                                        </td>
                                                        <td id="datanotes-{{ $key+1 }}">
                                                            {{ number_format($item->followers, 0, ',', '.') }}
                                                        </td>
                                                        <td>
                                                            {{ $item->engagement_detail ? number_format($item->engagement_detail->engagement, 2, ',', '.') : number_format($item->engagement_rate, 2, ',', '.') }}%
                                                        </td>
                                                        <td class="text-center">
                                                            <a href="#" class="btn btn-primary btn-detail" style="color: white"
                                                                data-id="{{ $key+1 }}"
                                                                data-igphoto="{{ $item->ig_photo }}"
                                                                data-igphotocarousel="{{ $item->ig_photo_carousel }}"
                                                                data-igstory="{{ $item->ig_story }}"
                                                                data-igvideo="{{ $item->ig_video }}"
                                                                data-eventattendance="{{ $item->event_attendance}}"
                                                                data-youtubevideo="{{ $item->youtube_video }}">
                                                                <span class="fas fa-search" title="detail"></span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<div class="modal fade" id="exportConfig" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Export Configuration</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="custom-paymentschedule-additional" class="active">
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="checkbox" name="col[]" value="phone_number">
                            <label> Phone Number</label><br>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="checkbox" name="col[]" value="email">
                            <label> Email</label><br>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="checkbox" name="col[]" value="ig_name">
                            <label> IG Name</label><br>
                            <input type="checkbox" name="col[]" value="ig_photo">
                            <label> IG Photo</label><br>
                            <input type="checkbox" name="col[]" value="ig_photo_carousel">
                            <label> IG Photo Carousel</label><br>
                            <input type="checkbox" name="col[]" value="ig_story">
                            <label> Ig Story</label><br>
                            <input type="checkbox" name="col[]" value="ig_video">
                            <label> Ig Video</label><br>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="checkbox" name="col[]" value="event_attendance">
                            <label> Event Attendance</label><br>
                            <input type="checkbox" name="col[]" value="youtube_channel">
                            <label> Youtube Channel</label><br>
                            <input type="checkbox" name="col[]" value="youtube_video">
                            <label> Youtube Video</label><br>
                            <input type="checkbox" name="col[]" value="followers">
                            <label> Followers</label><br>
                            <input type="checkbox" name="col[]" value="engagement_rate">
                            <label> Engagement Rate</label><br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary btn-confirm-export" type="submit">Submit</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('body').on('click', '.btn-export', function(e) {
            $('#exportConfig').modal('show');
        });

        $('body').on('click', '.btn-confirm-export', function(e) {
            e.preventDefault();

            var column = [];
            $('input[name^=col]:checkbox:checked').each(function(i){
                column[i] = $(this).val();
            });

            if(column.length < 1) {
                column = $('input[name^=col]').map(function(idx, elem) {
                    return $(elem).val();
                }).get();
            }

            $('#form-filter').find('#is_export').val("true");
            $('#form-filter').find('#export_column').val(column);
            $('#form-filter').submit();
            $('#exportConfig').modal('hide');
        });

        $('body').on('keyup', '.payment', function() {
            var value = convertToAngka($(this).val());

            $(this).val(convertToRupiah(value));
        });
    </script>
@endsection