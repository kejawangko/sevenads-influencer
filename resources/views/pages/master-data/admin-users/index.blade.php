@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Admin User</h1>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            for every new user, the password will be sent to their email.<br>
                            <strong>make sure the email is correct</strong>
                        </p>
                    </div>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <form action="{{ url('master-data/admin-users/store') }}" method="post">
                            @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Role</span>
                                </div>
                                <select class="form-control" name="role" id="role">
                                    <option disabled @if (old('role') == null) selected @endif>Pilih Role</option>
                                    <option value="superadmin" @if (old('role') == 'superadmin') selected @endif>Superadmin</option>
                                    <option value="admin" @if (old('role') == 'admin') selected @endif>Admin</option>
                                    <option value="finance" @if (old('role') == 'finance') selected @endif>Finance</option>
                                    <option value="kol" @if (old('role') == 'kol') selected @endif>Project KOL</option>
                                    <option value="dm" @if (old('role') == 'dm') selected @endif>Digital Marketing</option>
                                </select>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Name</span>
                                </div>
                                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Email</span>
                                </div>
                                <input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Phone Number</span>
                                </div>
                                <input type="text" name="phone_number" id="phone_number" class="form-control" value="{{ old('phone_number') }}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Date of Birth</span>
                                </div>
                                <input type="date" name="dob" id="dob" class="form-control" value="{{ old('dob') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Role</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>DOB</th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($items) < 1)
                                        <tr>
                                            <td colspan="99" class="text-center">No data</td>
                                        </tr>
                                    @endif
                                    @foreach ($items as $key => $item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td id="datauser_role{{ $key+1 }}">{{ $item->role }}</td>
                                            <td id="datauser_name{{ $key+1 }}">{{ $item->full_name }}</td>
                                            <td id="datauser_email{{ $key+1 }}">{{ $item->email }}</td>
                                            <td id="datauser_phone{{ $key+1 }}">{{ $item->phone_number }}</td>
                                            <td id="datauser_dob{{ $key+1 }}">{{ $item->dob }}</td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-warning btn-edit" style="color: white" data-id="{{ $item->id }}">
                                                    <span class="fas fa-edit" title="update"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="edit-user-form" action="#" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="role">Role</label>
                            <select name="role" id="edit-user-role" class="form-control">
                                <option value="superadmin">Superadmin</option>
                                <option value="admin">Admin</option>
                                <option value="finance">Finance</option>
                                <option value="kol">Project KOL</option>
                                <option value="dm">Digital Marketing</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="edit-user-name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="edit-user-email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" name="phone_number" id="edit-user-phone" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="dob">DOB</label>
                            <input type="date" name="dob" id="edit-user-dob" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.btn-edit').on('click', function(e) {
        e.preventDefault();
        
        var idUser = $(this).attr('data-id');
        var currentTr = $(this).closest('tr');
        var target = $('#editUserModal');

        var role = currentTr.find('#datauser_role'+idUser).text();
        var name = currentTr.find('#datauser_name'+idUser).text();
        var email = currentTr.find('#datauser_email'+idUser).text();
        var phone = currentTr.find('#datauser_phone'+idUser).text();
        var dob = currentTr.find('#datauser_dob'+idUser).text();

        target.find('#edit-user-role').val(role.toLowerCase());
        target.find('#edit-user-name').val(name);
        target.find('#edit-user-email').val(email);
        target.find('#edit-user-phone').val(phone);
        target.find('#edit-user-dob').val(dob);
        target.find('#edit-user-form').attr('action', "{{ url('master-data/admin-users/update') }}/"+idUser)

        target.modal('show');
    });
</script>
@endsection