@extends('layouts.app')
@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h4 class="mb-0 text-gray-800">Edit Categories</h4>
                </div>
                <div class="row py-3">
                    <div class="col-lg-12 py-3 bg-white">
                        <form action="{{ route('master.categories.update', ['id' => $item->id]) }}" method="post">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control" value="{{ $item->name }}">
                                </div>
                            </div>
                            <div class="form-group row mt-4">
                                <div class="col-sm-2 mb-3 mb-sm-0">
                                    <button class="btn btn-block btn-primary">
                                        Submit
                                    </button>
                                </div>
                                <div class="col-sm-2 mb-3 mb-sm-0">
                                    <a href="{{ route('master.categories') }}" class="btn btn-block btn-secondary">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@endsection

@section('script')
    <script>
    </script>
@endsection