@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                @include('includes.alert')
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Categories</h1>
                </div>
                <div class="row py-3 bg-white">
                    <div class="col-lg-12 mb-4">
                        <form action="{{ url('master-data/categories/store') }}" method="post">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Name</span>
                                </div>
                                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12 mb-4">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($items) < 1)
                                        <tr>
                                            <td colspan="4" class="text-center">No data</td>
                                        </tr>
                                    @endif
                                    @foreach ($items as $key => $item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>
                                                {{ $item->name }}
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ url('master-data/categories/edit/'.$item->id) }}" class="btn btn-warning" style="color: white">
                                                    <span class="fas fa-edit" title="update"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@endsection

@section('script')
<script>
</script>
@endsection