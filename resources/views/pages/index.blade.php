@extends('layouts.app')

@section('content')
<div id="wrapper">
    @include('includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('includes.navbar')
            <div class="container-fluid">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                </div>
                <div class="row mb-5">
                    <div class="col-lg-12">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 class="m-0 font-weight-bold text-uppercase text-gray-900">Calendar</h6>
                            </div>
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-lg-12">
                                        <div class="datepicker sidebar-datepicker"></div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h5>Payment on selected date</h5>
                                        <h4><strong>Total Payment : <span id="total-payment"><span>&#60;</span>select date<span>&#62;</span></span></strong></h4>
                                        <div class="table-responsive">
                                            <table class="table table-bordered dataTable" width="100%" cellspacing="0" id="detail-payment">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Due Date</th>
                                                        <th width="25%">Influencer Name</th>
                                                        <th>Bank Info</th>
                                                        <th>Amount</th>
                                                        <th>Invoice</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-4">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 class="m-0 font-weight-bold text-uppercase text-gray-900">Payment to be PAID</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Due Date</th>
                                                <th width="25%">Influencer Name</th>
                                                <th>Bank Info</th>
                                                <th>Amount</th>
                                                <th>Invoice</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($items as $key => $item)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ date('d F Y', strtotime($item->due_date)) }}</td>
                                                    <td>
                                                        {{ $item->projectInfluencers->influencer_name }}
                                                    </td>
                                                    <td>
                                                        Nama Pemilik Bank : 
                                                        <strong>{{ $item->projectInfluencers->bank_owner }}</strong><br>
                                                        Nama Bank : 
                                                        <strong>{{ $item->projectInfluencers->bank_name }}</strong><br>
                                                        Cabang Bank :
                                                        <strong>{{ $item->projectInfluencers->bank_branch_name }}</strong><br>
                                                        Nomor Rekening : 
                                                        <strong>{{ $item->projectInfluencers->bank_account }}</strong>
                                                    </td>
                                                    <td>
                                                        {{ number_format($item->total, 0, '.', ',') }}
                                                    </td>
                                                    <td>
                                                        <a href="{{ $item->projectInfluencers->invoice }}" target="_blank">{{ $item->projectInfluencers->invoice }}</a>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn-conf-payment" data-target="#paymentConfModal" 
                                                                data-url="{{ route('update-influencer-payment', ['type' => strtolower(str_replace(' ', '-', $item->projectInfluencers->projects->type)), 'id' => $item->id]) }}"
                                                                data-nameinf="{{ $item->projectInfluencers->influencer_name }}"
                                                                data-payment="{{ $item->total }}"
                                                                data-duedate="{{ date('d F Y', strtotime($item->due_date)) }}">paid</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mb-4">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 class="m-0 font-weight-bold text-uppercase text-gray-900">All Payments</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Due Date</th>
                                                <th width="25%">Influencer Name</th>
                                                <th>Bank Info</th>
                                                <th>Amount</th>
                                                <th>Invoice</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($all_payments as $key => $item)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ date('d F Y', strtotime($item->due_date)) }}</td>
                                                    <td>
                                                        {{ $item->projectInfluencers->influencer_name }}
                                                    </td>
                                                    <td>
                                                        Nama Pemilik Bank : 
                                                        <strong>{{ $item->projectInfluencers->bank_owner }}</strong><br>
                                                        Nama Bank : 
                                                        <strong>{{ $item->projectInfluencers->bank_name }}</strong><br>
                                                        Cabang Bank :
                                                        <strong>{{ $item->projectInfluencers->bank_branch_name }}</strong><br>
                                                        Nomor Rekening : 
                                                        <strong>{{ $item->projectInfluencers->bank_account }}</strong>
                                                    </td>
                                                    <td>
                                                        {{ number_format($item->total, 0, '.', ',') }}
                                                    </td>
                                                    <td>
                                                        <a href="{{ $item->invoice }}" target="_blank">{{ $item->invoice }}</a>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn-conf-payment" data-target="#paymentConfModal" 
                                                                data-url="{{ route('update-influencer-payment', ['type' => strtolower(str_replace(' ', '-', $item->projectInfluencers->projects->type)), 'id' => $item->id]) }}"
                                                                data-nameinf="{{ $item->projectInfluencers->influencer_name }}"
                                                                data-payment="{{ $item->total }}"
                                                                data-duedate="{{ date('d F Y', strtotime($item->due_date)) }}">paid</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<div class="modal fade" id="paymentConfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Payment Confirmation</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h4>Payment Detail</h4>
                <span id="inf-name"></span><br>
                Rp. <span id="inf-payment"></span><br>
                <span id="inf-duedate"></span><br><br>

                <strong>Are sure ? Once you update there's no turning back .</strong>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a href="#" id="btn-conf-payment-modal" class="btn btn-primary" type="submit">Yes</a>
            </div>
        </div>
    </div>
</div>

<style>
    .ui-datepicker {
        width: 100%;
    }

    .ui-datepicker td span, .ui-datepicker td a {
        padding: 30px 10px !important;
    }

    .ui-state-active  {
        border: none !important;
    }
    td.ui-state-active a {
        background-color: #5bb0ab !important;
        color: white;
    }
</style>
@endsection

@section('script')
<script>
    $('.btn-conf-payment').on('click', function(e) {
        var thisElement = $(this);
        var targetModal = thisElement.attr('data-target');
        var url = thisElement.attr('data-url');
        var nameInf = thisElement.attr('data-nameinf');
        var paymentSched = convertToRupiah(thisElement.attr('data-payment'));
        var dueDate = thisElement.attr('data-duedate');

        $(targetModal).find('#inf-name').text(nameInf)
        $(targetModal).find('#inf-payment').text(paymentSched)
        $(targetModal).find('#inf-duedate').text(dueDate)
        $(targetModal).find('#btn-conf-payment-modal').attr('href', url)
        $(targetModal).modal('show');
    })
    
    var events = [];
    @foreach($all_payments as $pymnt)
        events.push({Date: new Date("{{ date('m/d/Y', strtotime($pymnt->due_date)) }}")});
    @endforeach

    $('.datepicker').datepicker({
        minDate: 0,
        dateFormat: "yy-m-dd",
        yearRange: "-100:+20",
        displayEventEnd : true,
        beforeShowDay: function(date) {
            var result = [true, '', null];
            var matching = $.grep(events, function(event) {
                return event.Date.valueOf() === date.valueOf();
            });

            if (matching.length) {
                result = [true, 'ui-state-active', ''];
            }
            return result;
        },
        onSelect: function(dateText) {
            getPaymentDetail(dateText);
        }
    });

    function getPaymentDetail(date) {
        $.ajax({
            url: "{{ url('detail-date-payment') }}/"+date,
            method: 'get',
        }).done(function (result) {
            console.log(result);
            $('#detail-payment').find('tbody').html(result.data.data_table);
            $('#total-payment').html(result.data.total_payment);
        }).fail(function (err) {
            
        })
    }
</script>
@endsection