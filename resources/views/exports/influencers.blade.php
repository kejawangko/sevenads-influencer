<table>
    <thead>
        <tr>
            @foreach ($column as $col)
                <th>{{ ucwords(str_replace('_', ' ', $col)) }}</th>
                @if($col === 'ig_photo' || $col === 'ig_photo_carousel' || $col === 'ig_story' || $col === 'ig_video'
                    || $col === 'event_attendance' || $col === 'youtube_video')
                    <th>{{ ucwords(str_replace('_', ' ', $col)).' Mark Up' }}</th>
                @endif
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($items as $key => $item)
            <tr class="header">
                @foreach ($column as $col)
                    @if($col === 'ig_name')
                    <th><a href="https://instagram.com/{{ $item->ig_name}}" >{{ $item->ig_name}}</a></th>
                    @elseif($col === 'ig_photo' || $col === 'ig_photo_carousel' || $col === 'ig_story' || $col === 'ig_video'
                        || $col === 'event_attendance' || $col === 'youtube_video')
                        <th>{{ number_format($item->{$col}, 0, '.', ',') }}</th>
                        <td>{{ number_format(($item->{$col} * 1.2), 0, '.', ',') }}</td>
                    @else
                        <th>{{ $item->{$col} }}</th>
                    @endif
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>