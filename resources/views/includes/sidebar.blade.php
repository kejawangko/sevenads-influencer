<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-text mx-3">Admin Panel</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    @if(Auth::user()->role === 'superadmin' || Auth::user()->role === 'admin')
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Master Data
    </div>

    <li class="nav-item {{ (Request::is('master-data/admin-users') || Request::is('master-data/admin-users/*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('master-data/admin-users') }}">
            <i class="fas fa-fw fa-users"></i>
            <span>Admin User</span>
        </a>
    </li>

    <li class="nav-item {{ (Request::is('master-data/categories') || Request::is('master-data/categories/*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('master-data/categories') }}">
            <i class="fas fa-fw fa-list"></i>
            <span>Category</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <li class="nav-item {{ (Request::is('influencers') || Request::is('influencers/*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('influencers') }}">
            <i class="fas fa-fw fa-user"></i>
            <span>Influencer</span>
        </a>
    </li>
    @endif
    
    @if(Auth::user()->role === 'superadmin' || Auth::user()->role === 'admin' || Auth::user()->role === 'kol')
    <li class="nav-item {{ (Request::is('projects/kol') || Request::is('projects/kol/*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('projects/kol') }}">
            <i class="fas fa-fw fa-project-diagram"></i>
            <span>Project KOL</span>
        </a>
    </li>
    @endif

    @if(Auth::user()->role === 'superadmin' || Auth::user()->role === 'admin' || Auth::user()->role === 'dm')
    <li class="nav-item {{ (Request::is('projects/digital-marketing') || Request::is('projects/digital-marketing/*')) ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('projects/digital-marketing') }}">
            <i class="fas fa-fw fa-project-diagram"></i>
            <span>Digital Marketing</span>
        </a>
    </li>
    @endif

</ul>
<!-- End of Sidebar -->