<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\GetAuthLoginController')->name('login');
Route::post('login', 'Auth\PostAuthLoginController');
Route::get('logout', function () {
    Auth::logout();
    return redirect('/');
});

Route::get('/', 'GetDashboardController');
Route::get('detail-date-payment/{date}', 'GetDetailDatePaymentProjectController');

Route::prefix('master-data')->group(function () {
    Route::prefix('admin-users')->group(function () {
        Route::get('/', 'Master\AdminUser\GetMasterAdminUserController')->name('master.admin-users');
        Route::post('store', 'Master\AdminUser\PostMasterStoreAdminUserController')->name('master.admin-users.store');
        Route::post('update/{id}', 'Master\AdminUser\PostMasterUpdateAdminUserController')->name('master.admin-users.update');
    });

    Route::prefix('categories')->group(function () {
        Route::get('/', 'Master\Category\GetMasterCategoryController')->name('master.categories');
        Route::post('store', 'Master\Category\PostMasterStoreCategoryController')->name('master.categories.store');
        Route::get('edit/{id}', 'Master\Category\GetMasterEditCategoryController')->name('master.categories.edit');
        Route::post('update/{id}', 'Master\Category\PostMasterUpdateCategoryController')->name('master.categories.update');
        // Route::get('delete/{id}', 'Master\Category\GetMasterDeleteCategoryController')->name('master.categories.delete');
    });
});


Route::prefix('projects/{type}')->group(function () {
    Route::get('/', 'Project\GetProjectController');
    Route::post('store', 'Project\PostStoreProjectController');
    Route::get('{id}/detail', 'Project\GetDetailProjectController');
    Route::post('{id}/update', 'Project\PostUpdateProjectController');

    Route::post('{id}/influencer/store', 'Project\PostStoreProjectInfluencerController');
    Route::post('update-influencer-payment/{id}', 'Project\PostUpdateInfluencerPaymentProjectInfluencerController')->name('update-influencer-payment');
    Route::get('delete-influencer-payment/{id}', 'Project\GetDeleteInfluencerPaymentProjectInfluencerController')->name('delete-influencer-payment');
    Route::post('update-schedule-influencer-payment/{id}', 'Project\PostUpdateScheduleInfluencerPaymentProjectInfluencerController')->name('update-schedule-influencer-payment');

    Route::get('send-email-payment-schedules/{id}', 'Project\GetProjectSendEmailPaymentScheduleController');
});

Route::prefix('influencers')->group(function () {
    Route::get('/', 'Influencer\GetInfluencerController');
    Route::post('store', 'Influencer\PostStoreInfluencerController');
    Route::post('update/{id}', 'Influencer\PostUpdateInfluencerController');
    Route::post('import', 'Influencer\PostImportInfluencerController');
    Route::get('engagement-rate', 'Influencer\GetInfluencerEngagementRateController');
    Route::get('update-engagement-rate/{id}', 'Influencer\GetInfluencerUpdateEngagementRateController');
});

Route::get('send-email-payment-reminder', 'GetSendEmailPaymentReminderController');
// Route::get('finish-payment', function(Request $request) {
//     dd($request->all());
// });
// Route::post('finish-transaction', 'FinishTransaction');
